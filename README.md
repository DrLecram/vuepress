# Vuepress Projekt

Hier finden Sie wichtige Hinweise zur Bearbeitung Ihres Projektes.

## 1. [Webseiten Titel, Meta Angaben und Navigation bearbeiten](../../../_edit/main/docs/.vuepress/config.js)

## 2. Bearbeitung von Seiten
- [Startseite](../../../_edit/main/docs/README.md)
- [Aktuelles](../../../_edit/main/docs/aktuelles.md)
- ...
- [Impressum](../../../_edit/main/docs/impressum.md)
- [Datenschutz](../../../_edit/main/docs/datenschutz.md)
- [Kontakt](../../../_edit/main/docs/kontakt.md)

## 3. Inhalte Formatieren und verknüpfen
![Markdown Syntax](markdown-syntax.jpg)

## 4. Inhalte Archivieren
Inhalte können archiviert werden, indem die bestehende Datei umbenannt wird und eine neue erstellt wird.
Beispielsweise können Sie die Seite "Aktuelles" in verschiedene Jahre unterteilen, dazu am Besten einen Ordner "aktuelles" erstellen.

## 5. Bilder hochladen
Bilder sollten in das [Bilderverzeichnis](./docs/.vuepress/public/images) hochgeladen werden. Bei nutzung verschiedener Gallerien sollten Unterorderner erstellt werden.

## 6. Plugins nutzen
- [ ] Suche
- [ ] Kommentare
- [ ] Blog
- [ ] Mehrsprachigkeit
- [ ] Gallerie
- [ ] Social Media Leiste (Youtube, Facebook, Instagram, Tick Tok, usw.)
- [ ] Facebook Integration
- [ ] Youtube Integration
- [ ] Persönliche Seiten (Hash-Code)
- [ ] FAQ
- [ ] Google Maps / Open Street Maps
- [ ] Event Kalender (+ ical Download)
- [ ] Wetter
- [ ] QR-Code
- [ ] RSS-Feed
- [ ] Spenden

<https://github.com/vuepress/awesome-vuepress/blob/main/v2.md>