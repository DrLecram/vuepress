import { defineUserConfig, defaultTheme } from 'vuepress'
import { name, productName, description } from '../../package.json'

export default defineUserConfig({
    title: productName,
    description: description,
    dest: 'public',
    lang: 'de-DE',
    head: [],
    theme: defaultTheme({
      repo: `https://${name}.codeberg.page/admin/`,
      repoLabel: 'Login',
      editLinks: true,
      editLinkText: 'Bearbeiten',
      contributors: false,
      contributorsText: 'Bearbeitet von',
      lastUpdated: true,
      lastUpdatedText: 'Zuletzt geändert',
      navbar: [
        { text: 'Start', link: '/' },
        { text: 'Aktuelles', link: '/aktuelles' },
        { text: 'Kontakt', link: '/kontakt' }
      ],
      sidebar: false,
      logo: '/images/logo.svg'
    }),
    plugins: [],
  })
