import { defineClientConfig } from '@vuepress/client';
import Layout from './layouts/Layout.vue';
import Formular from './components/global/Formular.vue';

export default defineClientConfig({
  enhance({ app, router, siteData }) {
    app.component('Formular', Formular);
  },
  setup() {},
  rootComponents: [],
  layouts: {
    Layout,
  },
});
