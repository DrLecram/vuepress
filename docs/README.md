---
home: true
title: Vuepress
heroImage: /images/header.jpg
heroText: Vuepress
tagline: 
actions:
  - text: Aktuelles
    link: /aktuelles/
    type: primary
features:
  - title: Adresse
    address: xxx
  - title: Telefon
    phone: xxx
  - title: Öffnungszeiten
    details: xxx
---